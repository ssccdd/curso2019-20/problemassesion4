/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo2;

import static es.uja.ssccdd.curso1920.problemassesion4.grupo1.Constantes.aleatorio;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo2.Constantes.MIN_ESCENAS_X_GENERADOR;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo2.Constantes.MIN_TIEMPO_GENERACION;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo2.Constantes.VARIACION_ESCENAS_X_GENERADOR;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo2.Constantes.VARIACION_GENERACION;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tarea que simula la generacion de escenas
 * @author fconde
 */
public class GeneradorEscenas implements Callable<List<Escena>> {
    
    private final String id;
    private final ArrayList<Escena> arrayEscenas;
    private final ReentrantLock lock;

    public GeneradorEscenas(String id, ArrayList<Escena> arrayEscenas, ReentrantLock lock) {
        this.id = id;
        this.arrayEscenas = arrayEscenas;
        this.lock = lock;
    }

    @Override
    public List<Escena> call() {
        System.out.println("Ejecutando "+id);
        ArrayList<Escena> resultado = new ArrayList<Escena>();
        int numEscenas = MIN_ESCENAS_X_GENERADOR + aleatorio.nextInt(VARIACION_ESCENAS_X_GENERADOR);
        for (int i=0; i<numEscenas; i++) {
            int tiempo = MIN_TIEMPO_GENERACION+aleatorio.nextInt(VARIACION_GENERACION);
            try {
                TimeUnit.SECONDS.sleep(tiempo);
            } catch (InterruptedException ex) {
                Logger.getLogger(GeneradorEscenas.class.getName()).log(Level.SEVERE, null, ex);
            }
            Escena escena = new Escena(id+"-"+i);
            lock.lock();
            try {
                arrayEscenas.add(escena);
            } finally {
                lock.unlock();
            }
            resultado.add(escena);
        }
        System.out.println("Terminando ejecución "+id);
        return resultado;
    }
}
