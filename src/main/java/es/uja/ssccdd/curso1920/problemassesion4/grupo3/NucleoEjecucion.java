/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.EstadoEjecucion.CANCELADO;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.EstadoEjecucion.EN_EJECUCION;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.EstadoEjecucion.FINALIZADO;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.EstadoEjecucion.LISTO;
import es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.TipoProceso;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class NucleoEjecucion implements Callable<Proceso> {
    private final String iD;
    private final Proceso proceso;

    public NucleoEjecucion(String iD, TipoProceso tipoProceso) {
        this.iD = iD;
        this.proceso = new Proceso(iD.hashCode(),tipoProceso);
    }
    
    @Override
    public Proceso call() throws Exception {
        proceso.setEstado(LISTO);
        
        try {
            ejecucionProceso();
        } catch ( InterruptedException e ) {
            cancelacionProceso();
        }
        
        return proceso;
    }
    
    /**
     * Simula la ejecución de un proceso por parte de un núcleo del procesador
     * @throws InterruptedException 
     */
    private void ejecucionProceso() throws InterruptedException {
        proceso.setEstado(EN_EJECUCION);
        System.out.println("TAREA-" + iD + " Inicio ejecución " + proceso);
        
        // Simulamos la ejecución del proceso
        TimeUnit.SECONDS.sleep(proceso.tiempoEjecucion());
        proceso.setEstado(FINALIZADO);
        
        System.out.println("TAREA-" + iD + " Fin ejecución " + proceso);
    }
    
    /**
     * Se cancela la ejecución del proceso en el núcleo del procesador
     */
    private void cancelacionProceso() {
        proceso.setEstado(CANCELADO);
        System.out.println("TAREA-" + iD + " CANCELADA ejecución " + proceso);
    }

    public String getiD() {
        return iD;
    }    
}
