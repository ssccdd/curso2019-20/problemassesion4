/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo1;

import static es.uja.ssccdd.curso1920.problemassesion4.grupo1.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo1.Constantes.TOTAL_PROVEEDORES;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo1.Constantes.UNIDADES_PEDIDO;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService ejecucion;
        ArrayList<Proveedor> listaProveedores;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        listaProveedores = new ArrayList();
        ejecucion = Executors.newFixedThreadPool(TOTAL_PROVEEDORES);
        
        // Se preparan los pedidos para los proveedores y se inicia su ejecución
        for( int i = 0; i < TOTAL_PROVEEDORES; i++ ) {
            Proveedor proveedor = new Proveedor("Proveedor("+i+")",UNIDADES_PEDIDO);
            listaProveedores.add(proveedor);
        }
        
        // Se añaden al marco de ejecución y se espera a que finalice el primer pedido
        List<Ordenador> pedido = ejecucion.invokeAny(listaProveedores);
        
        // Finalizamos el ejecutor y esperamos a que todas las tareas finalicen
        System.out.println("HILO(Principal) Espera a la finalización de los pedidos");
        ejecucion.shutdownNow();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        // Presentamos el resultado del pedido
        System.out.println("HILO(Principal) el pedido obtenido es \n______________");
        for( Ordenador ordenador : pedido )
            System.out.println("\t" + ordenador);
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
