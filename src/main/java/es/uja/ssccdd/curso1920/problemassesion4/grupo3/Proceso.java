/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo3;

import es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.EstadoEjecucion;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.EstadoEjecucion.CREADO;
import es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.TipoProceso;


/**
 *
 * @author pedroj
 */
public class Proceso {
    private final int iD;
    private final TipoProceso proceso;
    private EstadoEjecucion estado;

    public Proceso(int iD, TipoProceso proceso) {
        this.iD = iD;
        this.proceso = proceso;
        this.estado = CREADO;
    }

    public int getiD() {
        return iD;
    }

    public TipoProceso getProceso() {
        return proceso;
    }

    public EstadoEjecucion getEstado() {
        return estado;
    }

    public void setEstado(EstadoEjecucion estado) {
        this.estado = estado;
    }
    
    /**
     * Tiempo de ejecución del proceso
     * @return 
     *      los segundos de ejecución
     */
    public int tiempoEjecucion() {
        return proceso.tiempoEjecucion();
    }

    @Override
    public String toString() {
        return "Proceso{" + "iD=" + iD + ", proceso=" + proceso + ", estado=" + estado + '}';
    }
}
