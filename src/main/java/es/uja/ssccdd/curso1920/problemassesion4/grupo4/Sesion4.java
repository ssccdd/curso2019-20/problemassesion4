/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion4.grupo4.Constantes.NUM_ETAPAS;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo4.Constantes.PROMOTORES;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo4.Constantes.TIEMPO_ESPERA;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService ejecucion;
        ArrayList<Promotor> listaPromotores;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        listaPromotores = new ArrayList();
        ejecucion = Executors.newFixedThreadPool(PROMOTORES);
        
        // Se preparan los pedidos para los promotores y se inicia su ejecución
        for( int i = 0; i < PROMOTORES; i++ ) {
            Promotor promotor = new Promotor("Promotor("+i+")",NUM_ETAPAS);
            listaPromotores.add(promotor);
        }
        
        // Se añaden al marco de ejecución y se espera a que finalice el primer pedido
        List<Casa> listaProcesos = ejecucion.invokeAny(listaPromotores);
        
        // Finalizamos el ejecutor y esperamos a que todas las tareas finalicen
        System.out.println("HILO(Principal) Espera a la finalización de la instalación");
        ejecucion.shutdownNow();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        // Presentamos el resultado del pedido
        System.out.println("HILO(Principal) las casas instaladas son \n______________");
        for( Casa casa : listaProcesos )
            System.out.println("\t" + casa);
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }   
}
