/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo2;

import static es.uja.ssccdd.curso1920.problemassesion4.grupo2.Constantes.NUM_GENERADORES;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo2.Constantes.NUM_RENDERIZADORES;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author fconde
 */
public class Sesion4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println("** Hilo(PRINCIPAL): Ha iniciado la ejecucion\n");
        
        ArrayList<Escena> arrayEscenas = new ArrayList<Escena>();
        ReentrantLock lock = new ReentrantLock();

        // - Construir el marco de ejecucion con numero maximo de hilos fijo.
        //   Construir todas las tareas (tanto generadores como renderizadores)
        ExecutorService executor = Executors.newFixedThreadPool(NUM_GENERADORES+NUM_RENDERIZADORES);
        ArrayList<Callable<List<Escena>>> tareas = new ArrayList<Callable<List<Escena>>>();
        for (int i=0; i<NUM_GENERADORES; i++) {
            GeneradorEscenas generador = new GeneradorEscenas("GEN-"+i, arrayEscenas, lock);
            tareas.add(generador);
        }
        for (int i=0; i<NUM_RENDERIZADORES; i++) {
            RenderizadorEscenas renderizador = new RenderizadorEscenas("REND-"+i, arrayEscenas, lock);
            tareas.add(renderizador);
        }
        
        // - Construir la lista de Futures que recibiran los resultados de las
        //   tareas (en este ejemplo, las escenas que hayan generado / renderizado)
        //   Ejecutar las tareas esperando a que terminen todas ellas.
        List<Future<List<Escena>>> resultados = null;
        try {
            resultados = executor.invokeAll(tareas);
        } catch (InterruptedException ex) {
            Logger.getLogger(Sesion4.class.getName()).log(Level.SEVERE, null, ex);
        }

        // - Hacer que el marco de ejecucion no pueda recibir nuevas tareas
        executor.shutdown();

        System.out.println("\n** Hilo(PRINCIPAL): Todos los hilos han terminado\n");

        // - En este punto todos los hilos han terminado. Presentar los resultados.
        for (int i=0; i<resultados.size(); i++) {
            Future<List<Escena>> resultado = resultados.get(i);
            List<Escena> res;
            try {
                res = resultado.get();
                System.out.println(res);
            } catch (InterruptedException ex) {
                Logger.getLogger(Sesion4.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ExecutionException ex) {
                Logger.getLogger(Sesion4.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        // - Un ArrayList puede imprimirse directamente si los elementos que
        //   contienen han definido el metodo toString()
        System.out.println("\nEscenas sin procesar: "+arrayEscenas+"\n");

        System.out.println("** Hilo(PRINCIPAL): Ha finalizado la ejecucion");
    }    
}
