/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion4.grupo1.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.NUCLEOS;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.TIEMPO_GESTION;
import es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.TipoProceso;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class UnidadProcesamiento implements Callable<List<Proceso>> {
    private final String iD;
    private final int numCiclos;
    private final ArrayList<Proceso> listaProcesos;
    private final ExecutorService ejecucion;

    public UnidadProcesamiento(String iD, int numCiclos) {
        this.iD = iD;
        this.numCiclos = numCiclos;
        this.listaProcesos = new ArrayList();
        this.ejecucion = Executors.newFixedThreadPool(NUCLEOS);
    }
    
    
    @Override
    public List<Proceso> call() throws Exception {
        System.out.println("TAREA-" + iD + " Empieza la ejecución de sus " 
                            + numCiclos + " ciclos asignados");
        
        try {
            for(int i = 0; i < numCiclos; i++) {
                ejecucion(i);
            }
            
            System.out.println("TAREA-" + iD + " Finaliza la ejecución de sus " 
                            + numCiclos + " ciclos asignados");
        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " CANCELA la ejecución de sus " 
                            + numCiclos + " ciclos asignados");
        } finally {
            ejecucion.shutdownNow();
            ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        }
        
        return listaProcesos;
    }

    private void ejecucion(int ciclo) throws InterruptedException, ExecutionException {
        List<Future<Proceso>> resultadoEjecucion;
        ArrayList<NucleoEjecucion> listaTareas = new ArrayList();
        
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        System.out.println("TAREA-" + iD + " Se inicia el ciclo " + (ciclo+1) 
                           + " del procesador");
        
        // Creamos los núclos de ejecución que tendrán un TipoProceso aleatorio
        // que ejecutar
        
        for(int i = 0; i < NUCLEOS; i++) {
            NucleoEjecucion nucleo = new NucleoEjecucion(iD+"("+i+")",
                                                         TipoProceso.getProceso());
            listaTareas.add(nucleo);                                                
        }
        
        // Añadimos las tareas asociadas a los núcleos de ejecución y esperamos
        // a que finalicen todas
        resultadoEjecucion = ejecucion.invokeAll(listaTareas);
        
        // Recojemos los procesos finalizados y los añadimos a la lista
        for( Future<Proceso> proceso : resultadoEjecucion )
            listaProcesos.add(proceso.get());

        // Simulamos tiempo de gestión interna antes de completar el ciclo de ejecución
        TimeUnit.SECONDS.sleep(aleatorio.nextInt(TIEMPO_GESTION));
        
        System.out.println("TAREA-" + iD + " Finaliza el ciclo " + (ciclo+1) 
                           + " del procesador");
    }
    
    public String getiD() {
        return iD;
    }
}
