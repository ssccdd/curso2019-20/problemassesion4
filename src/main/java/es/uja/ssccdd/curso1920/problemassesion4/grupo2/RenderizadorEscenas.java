/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo2;

import static es.uja.ssccdd.curso1920.problemassesion4.grupo1.Constantes.aleatorio;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo2.Constantes.MIN_ESCENAS_X_RENDERIZADOR;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo2.Constantes.VARIACION_ESCENAS_X_RENDERIZADOR;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Tarea que simula la renderizacion de escenas.
 * @author fconde
 */
public class RenderizadorEscenas implements Callable<List<Escena>> {

    private final String id;
    private final ArrayList<Escena> arrayEscenas;
    private final ReentrantLock lock;

    public RenderizadorEscenas(String id, ArrayList<Escena> arrayEscenas, ReentrantLock lock) {
        this.id = id;
        this.arrayEscenas = arrayEscenas;
        this.lock = lock;
    }    

    @Override
    public List<Escena> call() {
        System.out.println("Ejecutando "+id);
        ArrayList<Escena> resultado = new ArrayList<Escena>();
        Escena escena = null;
        boolean tengoEscena;
        int numEscenas = MIN_ESCENAS_X_RENDERIZADOR + aleatorio.nextInt(VARIACION_ESCENAS_X_RENDERIZADOR);
        int j=0;
        while (j<numEscenas) {
            tengoEscena = false;
            lock.lock();
            try {
                if (!arrayEscenas.isEmpty()) {
                    escena = arrayEscenas.remove(0);
                    j++;
                    resultado.add(escena);
                    tengoEscena = true;
                }
            } finally {
                lock.unlock();
            }
            
            if (tengoEscena) {
                try {
                    TimeUnit.SECONDS.sleep(escena.getDuracion());
                } catch (InterruptedException ex) {
                    Logger.getLogger(RenderizadorEscenas.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        System.out.println("Terminando ejecución "+id);
        return resultado;
    }
    
}
