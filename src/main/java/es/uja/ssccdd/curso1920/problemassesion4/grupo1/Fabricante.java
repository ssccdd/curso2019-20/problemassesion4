/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo1;

import es.uja.ssccdd.curso1920.problemassesion4.grupo1.Constantes.TipoComponente;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Fabricante implements Callable<Componente> {
    private final String iD;
    private final TipoComponente tipoComponente;

    public Fabricante(String iD, TipoComponente tipoComponente) {
        this.iD = iD;
        this.tipoComponente = tipoComponente;
    }

    @Override
    public Componente call() throws Exception {
        Componente resultado=null;
        
        System.out.println("TAREA-" + iD + " Fabrica su componente " + tipoComponente);
        
        try {
            resultado = prepararComponente();
            
            System.out.println("TAREA-" + iD + " Finaliza el componente " + tipoComponente);
        } catch ( InterruptedException e ) {
            System.out.println("TAREA-" + iD + " Se ha cancelado la fabricación");
        }
        
        return resultado;
    }
    
    private Componente prepararComponente() throws InterruptedException {
        // Fabrica un nuevo componente
        Componente resultado = new Componente(iD+"-"+tipoComponente.ordinal(), tipoComponente);
        
        // Simula la fabricación del componente
        TimeUnit.SECONDS.sleep(resultado.tiempoFabricacion());
        
        return resultado;
    }

    public String getiD() {
        return iD;
    }
}
