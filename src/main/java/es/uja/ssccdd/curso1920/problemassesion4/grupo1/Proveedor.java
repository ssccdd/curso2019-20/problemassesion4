/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo1;

import static es.uja.ssccdd.curso1920.problemassesion4.grupo1.Constantes.COMPONENTES;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo1.Constantes.TIEMPO_ESPERA;
import es.uja.ssccdd.curso1920.problemassesion4.grupo1.Constantes.TipoComponente;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Proveedor implements Callable<List<Ordenador>> {
    private final String iD;
    private final int unidadesPedido;
    private ArrayList<Ordenador> pedido;
    private final ExecutorService ejecucion;

    public Proveedor(String iD, int unidadesPedido) {
        this.iD = iD;
        this.unidadesPedido = unidadesPedido;
        this.pedido = new ArrayList();
        
        // Marco ejecución para los fabricantes del proveedor
        this.ejecucion = Executors.newFixedThreadPool(COMPONENTES.length);
    }
    
    @Override
    public List<Ordenador> call() throws Exception {
        System.out.println("TAREA-" + iD + " Empieza ha preparar su pedido de " 
                            + unidadesPedido + " ordenadores");
        
        try {
            for( int i = 0; i < unidadesPedido; i++ )
                montarOrdenador(i);
            
            System.out.println("TAREA-" + iD + " Ha finalizado su pedido de " 
                            + unidadesPedido + " ordenadores");
        } catch ( InterruptedException e ) {
            System.out.println("TAREA-" + iD + " Se ha CANCELADO el PEDIDO");
        } finally {
            ejecucion.shutdownNow();
            ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        }
        
        return pedido;
    }
    
    private void montarOrdenador(int unidad) throws InterruptedException, ExecutionException {
        List<Future<Componente>> resultadoEjecucion;
        ArrayList<Fabricante> listaTareas = new ArrayList();
        
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        System.out.println("TAREA-" + iD + " Solicita los componetes para el ordenador " 
                           + (unidad+1) + " del pedido de " + unidadesPedido);
        for(TipoComponente tipoComponente : COMPONENTES ) {
            Fabricante fabricante = new Fabricante(iD+"-"+tipoComponente.name(),
                                                   tipoComponente);
            listaTareas.add(fabricante);
        }
        
        // Espera a la fabricación de todos los componentes del ordenador
        resultadoEjecucion = ejecucion.invokeAll(listaTareas);
        
        System.out.println("TAREA-" + iD + " Se está montando el ordenador " 
                           + (unidad+1) + " del pedido de " + unidadesPedido);
        // Se monta el ordenador con los componentes fabricados
        Ordenador ordenador = new Ordenador();
        ordenador.setiD(iD+"-"+(unidad+1));
        for( Future<Componente> fabricado : resultadoEjecucion ) {
            Componente componente = fabricado.get();
            ordenador.addComponente(componente);
        }
           
        // Se simula el tiempo de montaje
        TimeUnit.SECONDS.sleep(ordenador.tiempoMontaje());
        pedido.add(ordenador);
    }

    public String getiD() {
        return iD;
    }
}
