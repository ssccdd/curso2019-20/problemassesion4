/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo4;

import static es.uja.ssccdd.curso1920.problemassesion4.grupo4.Constantes.INSTALADORES;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo4.Constantes.NUM_SENSORES;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo4.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo4.Constantes.VARIACION;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo4.Constantes.aleatorio;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Promotor implements Callable<List<Casa>> {
    private final String iD;
    private final int numEtapas;
    private final ArrayList<Casa> listaCasas;
    private final ExecutorService ejecucion;

    public Promotor(String iD, int numEtapas) {
        this.iD = iD;
        this.numEtapas = numEtapas;
        this.listaCasas = new ArrayList();
        this.ejecucion = Executors.newFixedThreadPool(INSTALADORES);
    }
    
    @Override
    public List<Casa> call() throws Exception {
        System.out.println("TAREA-" + iD + " Empieza la instalación de sus " 
                            + numEtapas + " etapas asignadas");
        
        try {
            
            for(int i = 0; i < numEtapas; i++) 
                instalarEtapa(i);
            
            System.out.println("TAREA-" + iD + " Finaliza la instalación de sus " 
                            + numEtapas + " etapas asignadas");
        } catch (InterruptedException ex) {
            System.out.println("TAREA-" + iD + " CANCELA la instalación de sus " 
                            + numEtapas + " etapas asignadas");
        } finally {
            ejecucion.shutdownNow();
            ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        }
        
        return listaCasas;
    }
    
    private void instalarEtapa(int etapa) throws InterruptedException, ExecutionException {
        List<Future<Casa>> resultadoEjecucion;
        ArrayList<Instalador> listaTareas = new ArrayList();
        int numSensores;
        
        if( Thread.currentThread().isInterrupted() )
            throw new InterruptedException();
        
        System.out.println("TAREA-" + iD + " Empieza la " + (etapa+1) + "ª etapa" +
                            " de un total de " + numEtapas + " etapas");
        
        
        // Se crean los instaladores y se inicia su ejecución para esta etapa
        for(int i = 0; i < INSTALADORES; i++) {
            numSensores = aleatorio.nextInt(VARIACION) + NUM_SENSORES;
            Instalador instalador = new Instalador(iD+"("+i+")",numSensores);
            listaTareas.add(instalador);
        }
        
        // Esperamos a que finalicen los instaladores antes se pasar a otra etapa
        resultadoEjecucion = ejecucion.invokeAll(listaTareas);
        
        // Actualizamos la lista de casas instaladas
        for( Future<Casa> casa : resultadoEjecucion )
            listaCasas.add(casa.get());
        
        System.out.println("TAREA-" + iD + " Finaliza la " + (etapa+1) + "ª etapa" +
                            " de un total de " + numEtapas + " etapas");
    }

    public String getiD() {
        return iD;
    }
}
