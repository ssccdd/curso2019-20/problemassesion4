/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo3;

import java.util.Random;

/**
 *
 * @author pedroj
 */
public interface Constantes {
    // Generador aleatorio
    public static final Random aleatorio = new Random();
    
    public enum TipoProceso {
        INTENSIVO(25,5), LIGERO(50,1), MIXTO(100,3);
        
        private final int valor;
        private final int tiempoEjecucion; // Base del tiempo de ejecución 

        private TipoProceso(int valor, int tiempoEjecucion) {
            this.valor = valor;
            this.tiempoEjecucion = tiempoEjecucion;
        }

        /**
         * Nos proporciona un TipoProceso aleatorio
         * @return
         *      proceso aleatorio generado
         */
        public static TipoProceso getProceso() {
            int valor = aleatorio.nextInt(D100); 
            TipoProceso resultado = null;
            int i = 0;
            
            while( (i < PROCESOS.length) && (resultado == null) ) {
                if ( PROCESOS[i].valor > valor )
                    resultado = PROCESOS[i];
                
                i++;
            }
            
            return resultado;
        }
        
        /**
         * Simula el tiempo de ejecución que tendrá el tipo de proceso con una 
         * variación prevista por la constante VARIACION
         * @return 
         *      tiempo de ejecución del proceso en unidades
         */
        public int tiempoEjecucion() {
            return aleatorio.nextInt(VARIACION) + tiempoEjecucion;
        }
    }
    
    public enum EstadoEjecucion {
        CREADO, LISTO, EN_EJECUCION, BLOQUEADO, CANCELADO, FINALIZADO;
    }
    
    // Constantes del problema
    public static final int TOTAL_PROCESADORES = 4;
    public static final int NUCLEOS = 4;
    public static final int INICIO = 1;
    public static final int D100 = 100; // Simula un dado de 100 caras
    public static final TipoProceso[] PROCESOS = TipoProceso.values() ;
    public static final int VARIACION = 4; // Variación para el tiempo de ejecución
    public static final int TIEMPO_GESTION = 2;
    public static final int TIEMPO_ESPERA = 30; // segundos
    public static final int NUM_CICLOS = 5; // Maximo de procesos de cada tipo
    
}
