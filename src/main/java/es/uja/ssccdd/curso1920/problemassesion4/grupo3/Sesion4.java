/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo3;

import static es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.TIEMPO_ESPERA;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.NUM_CICLOS;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo3.Constantes.TOTAL_PROCESADORES;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author pedroj
 */
public class Sesion4 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException, ExecutionException {
        ExecutorService ejecucion;
        ArrayList<UnidadProcesamiento> listaProcesadores;
        
        // Ejecución del hilo principal
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        // Inicialización de las variables para la prueba
        listaProcesadores = new ArrayList();
        ejecucion = Executors.newFixedThreadPool(TOTAL_PROCESADORES);
        
        // Se preparan los pedidos para los proveedores y se inicia su ejecución
        for( int i = 0; i < TOTAL_PROCESADORES; i++ ) {
            UnidadProcesamiento procesador = new UnidadProcesamiento("Procesador("+i+")",NUM_CICLOS);
            listaProcesadores.add(procesador);
        }
        
        // Se añaden al marco de ejecución y se espera a que finalice el primer pedido
        List<Proceso> listaProcesos = ejecucion.invokeAny(listaProcesadores);
        
        // Finalizamos el ejecutor y esperamos a que todas las tareas finalicen
        System.out.println("HILO(Principal) Espera a la finalización de los procesos");
        ejecucion.shutdownNow();
        ejecucion.awaitTermination(TIEMPO_ESPERA, TimeUnit.DAYS);
        
        // Presentamos el resultado del pedido
        System.out.println("HILO(Principal) los procesos finalizados \n______________");
        for( Proceso proceso : listaProcesos )
            System.out.println("\t" + proceso);
        
        // Finalización del hilo principal
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }
    
}
