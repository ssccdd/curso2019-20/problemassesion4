/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uja.ssccdd.curso1920.problemassesion4.grupo2;

import static es.uja.ssccdd.curso1920.problemassesion4.grupo1.Constantes.aleatorio;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo2.Constantes.MIN_DURACION_ESCENA;
import static es.uja.ssccdd.curso1920.problemassesion4.grupo2.Constantes.VARIACION_DURACION;

/**
 * Escena. No hay que añadirle nada mas. En este ejercicio, de la escena solo
 * interesa poder distinguir unas de otras por su nombre.
 * @author fconde
 */
public class Escena {
    
    private final String nombre;
    private final int duracion;

    public Escena(String nombre) {
        this.nombre = nombre;
        this.duracion = MIN_DURACION_ESCENA + aleatorio.nextInt(VARIACION_DURACION);
    }

    public String getNombre() {
        return nombre;
    }
    
    public int getDuracion() {
        return duracion;
    }

    @Override
    public String toString() {
        return "Escena(" + nombre + ", dur: " + duracion + ")";
    }
}
