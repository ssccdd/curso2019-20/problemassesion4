[![logo](https://www.gnu.org/graphics/gplv3-127x51.png)](https://choosealicense.com/licenses/gpl-3.0/)
# Problemas Prácticas
## Sesión 4

Problemas propuestos para la Sesión 4 de prácticas de la asignatura de Sistemas Concurrentes y Distribuidos del Grado en Ingeniería Informática de la Universidad de Jaén en el curso 2019-20.

En estos ejercicios se utilizarán las interfaces [`Executors`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Executor.html) y [`ExecutorService`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/ExecutorService.html) para la ejecución de las tareas. Las tareas se definirán como clases que implementen la interface [`Callable<V>`](https://docs.oracle.com/javase/8/docs/api/java/util/concurrent/Callable.html). Los ejercicios son diferentes para cada grupo:

 - [Grupo 1](https://gitlab.com/ssccdd/curso2019-20/problemassesion4#grupo-1)
 - [Grupo 2](https://gitlab.com/ssccdd/curso2019-20/problemassesion4#grupo-2)
 - [Grupo 3](https://gitlab.com/ssccdd/curso2019-20/problemassesion4#grupo-3)
 - [Grupo 4](https://gitlab.com/ssccdd/curso2019-20/problemassesion4#grupo-4)

## Grupo 1

El ejercicio consiste en la realización de unos pedidos de ordenadores por parte de una lista de proveedores. Cada uno de los proveedores contactará con sus fabricantes que le suministran los componentes necesarios para la construcción de los ordenadores. Los proveedores indicarán a los fabricantes que les suministren las piezas para la realización de un ordenador. Cada fabricante devolverá un componente a la finalización de su ejecución. El proveedor irá montanto ordenador a ordenador del pedido, es decir, no pedirá las piezas para un nuevo ordenador hasta que no finalice con la construcción del ordenador en curso. Para la ejecución de las tareas será necesario un marco `ExecutorService` y las herramientas de sincronización que nos proporciona. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Componente`: Clase que representa a un componente de un `TipoComponente` para la construcción de un `Ordenador`.
- `Ordenador`: Representa el ordenador que estará compuesto por un `Componente` de los presentes en `TipoComponente`. En otro caso se considera inacabado.

El ejercicio consiste en completar la implementación de las siguientes clases que se presentan. Para el desarrollo del ejercicio las únicas herramientas de sincronización permitidas son las que nos proporciona el **marco de ejecución**:

- `Fabricante`: Tiene un identificador asociado y un `TipoComponente` que es capaz de fabricar. El trabajo del montador es:
	- Mientas no se solicite su interrupción
		- Fabricar el `Componente` asociado.
		- Simula el tiempo de fabricación del `Componente`.
	- Tiene que indicar si ha finalizado correctamente la fabricación o a sido interrumpido.
	- El `Componente` fabricado es el resultado de su ejecución..

- `Proveedor`: Debe tener un identificador para diferenciarlo y un valor para las unidades que compondrán el pedido de ordenadores. El trabajo del proveedor es:
	- Se monta un ordenador del pedido cada vez, y para ello:
		- Creará una tarea para asociar un `Fabricante` a un `TipoComponente` para el `Ordenador`.
		- Ejecutará las tareas y esperará a que finalicen todas ellas.
		- Recuperará el `Componente` de cada `Fabricante` para añadirlos al `Ordenador`.
		- Simulará el tiempo de montaje del ordenador.
		- Se añadirá el `Ordenador` a la lista del pedido.
	- Debe comprobar que no se ha solicitado la cancelación del pedido de ordenadores.
	- Hay que indicar si ha finalizado correctamente el pedido o ha sido cancelado.
	- Antes de finalizar debe finalizar la ejecución del marco `Executor` y esperar a que todos los `Fabricante` finalicen mediante el método `awaitTermination(.)`.
	- El pedido de ordenadores es el resultado de la ejecución de esta tarea.

- `Hilo princial`: Realizará los siguientes pasos:
	- Crear un marco `Executor` para `TOTAL_PROVEEDORES`.
	- Para cada `Proveedor`:
		- Tiene que realizar un pedido de `UNIDADES_PEDIDO` .
	- Añadir los los proveedores al marco `Executor`.
	- Espera a que el primer `Proveedor` finalice su pedido de ordenadores.
	- Finalizar el marco `Executor` para que se cancele el resto de pedidos.
	- Esperar a que todos los proveedores finalicen mediante el método `awaitTermination(.)`.
	- Presentar la lista del pedido que ha finalizado primero antes de finalizar la ejecución del hilo principal.

## Grupo 2

En una granja de rendering (render farm) se generan escenas que luego se renderizan. Todo el proceso se realiza concurrentemente. Hay generadores de escenas que recopilan todo lo necesario para renderizar una escena (modelos 3D, texturas, materiales, etc.) y lo ponen a disposición de los renderizadores que construyen las imágenes con la especificación de la escena, mediante una lista de escenas común. (En esta sesión las tareas son simuladas con sleep).

Para la realización de este ejercicio no será necesario crear ninguna clase, pero se deberá terminar la implementación de que se indican más adelante. Se usarán marcos de ejecución para controlar la **creación/ejecución/sincronización** de los hilos, en concreto alguna implementación de  `Executor`  que garantice que no se  van a generar nunca mas de `NUM_GENERADORES+NUM_RENDERIZADORES` hilos. Además, se utilizará `ReentrantLock` para resolver los problemas de exclusión mutua que se presenten. En este primer ejercicio con marcos de ejecución no se cancelan los hilos.

Para la solución se tienen que utilizar obligatoriamente los siguientes elementos ya programados:

- `Constantes`: Interface con las constantes y los elementos auxiliares necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Escena`: Representa a una escena que se tiene que renderizar. Tiene asociado un tiempo de renderización que se genera aleatoriamente al construir la escena.

En el programa se tienen las siguientes tareas:

-   `GeneradorEscena`
    -   Tiene acceso a la lista de escenas pendientes.
    -   Genera Escenas y las añade a la lista.
        -   La generación de una escena consume un tiempo aleatorio de entre `MIN_TIEMPO_GENERACION` y `MIN_TIEMPO_GENERACION+VARIACION_GENERACION` segundos.
        -   La tarea de generar escenas termina cuando se hayan generado un número aleatorio de escenas comprendido entre `MIN_ESCENAS_X_GENERADOR` y `MIN_ESCENAS_X_GENERADOR + VARIACION_ESCENAS_X_GENERADOR`.
        -   Cuando se completa la tarea se devuelve una lista con las escenas que se han generado.
-   `RenderizadorEscena`
    -   Tiene acceso a la lista de escenas pendientes.
    -   Selecciona la siguiente escena disponible y la renderiza.
        -   La renderización de una escena consume un tiempo de entre `MIN_DURACION_ESCENA` y `MIN_DURACION_ESCENA + VARIACION_DURACION` que se genera aleatoriamente  **al construir la escena.**
        -   La tarea de renderizar escenas finaliza cuando se completan un número aleatorio de escenas comprendido entre `MIN_ESCENAS_X_RENDERIZADOR` y `MIN_ESCENAS_X_RENDERIZADOR + VARIACION_ESCENAS_X_RENDERIZADOR` escenas.
        -   Cuando se completa la tarea se devuelve una lista con las escenas que se han procesado.
-   `Hilo Principal`
    -   Crea la lista de escenas pendientes.
    -   Crea e inicia la ejecución de `NUM_GENERADORES` Tareas GeneradorEscenas.
    -   Crea e inicia la ejecución de `NUM_RENDERIZADORES` Tareas RenderizadorEscenas.
    -   Esperará hasta que todas las tareas estén completas.
    -   Muestra para cada Tarea Generador de escenas y cada Tarea Renderizador de escenas las escenas que ha generado / renderizado.
    -   Muestra las escenas que hayan quedado sin renderizar en la lista de escenas pendientes.

## Grupo 3

El ejercicio consistirá en simular una serie de ciclos de ejecución para unas unidades de procesamiento que tendrá asociados sus propios núcleos de ejecución independientes de otras unidades de procesamiento. Para cada ciclo de ejecución los núcleos ejecutarán un tipo de proceso aleatorio y hasta que todos los núcleos no finalicen no puede la unidad de procesamiento pasar al ciclo siguiente de ejecución. El hilo principal será el encargado de crear las unidades de procesamiento y cada una de ellas creará sus núcleos de ejecución. Para la ejecución de las tareas será necesario un marco `ExecutorService` y las herramientas de sincronización que nos proporciona. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Proceso`: Representa al proceso que simulará su ejecución en una unidad de procesamiento.

El ejercicio consiste en completar la implementación de las siguientes clases:

- `NucleoEjecucion`: Tendrá asociado un identificador, un `TipoProceso` que ejecutará.
	- Se simulará la ejecución del proceso.
	- Hay que programar la solicitud de interrupción.
	- La tarea debe indicar si finalizó correctamente o ha sido interrumpida.
	- A la finalización el Proceso será el elemento calculado en su ejecución.

- `UnidadProcesamiento`: Tendrá asociado un identificador, el número de ciclos que debe realizar.
	- Para cada ciclo de ejecución:
		- Creará tantos `NucleEjecucion` como los indicados en `NUCLEOS`
			- Cada núcleo tiene un `TipoProceso` asociado.
		- Espera a que todos los núcleos finalicen la ejecución de su Proceso.
		- Almacenará en una lista los procesos que han ejecutado los núcleos y lo simula con un `TIEMPO_GESTION` aleatorio .
	- Se debe programar la solicitud de interrupción. Tiene que solicitar la interrupción de sus núcleos de procesamiento.
	- A la finalización, se debe indicar si ha sido normal o cancelada, y esperará a que todos los núcleos finalicen mediante el método `awaitTermination(.)`.
	- El resultado de la ejecución es la lista de `Proceso` que ha completado.

- `Hilo principal`: Realizará las siguientes tareas:
	- Creará un marco de ejecución con una capacidad de `TOTAL_PROCESADORES`
	- Para cada unidad de procesamiento:
		- Tendrá un número de ciclos indicado por `NUM_CICLOS`.
	- Añadir las unidades de procesamiento al marco `Executor`.
	- Espera a que la primera `UnidadProcesamiento` finalice sus ciclos de ejecución.
	- Finalizar el marco `Executor` para que se cancele el resto de unidades de procesamiento.
	- Esperar a que todas las tareas finalicen mediante el método `awaitTermination(.)`.
	- Presentar la lista de procesos que ha completado la `UnidadProcesamiento` antes de finalizar la ejecución del hilo principal.


## Grupo 4

El ejercicio consiste en que un número de promotores deberán completar una serie de etapas. Para cada etapa y promotor hay un número de instaladores asignado cada uno de ellos a una casa en la que instalarán un número de sensores. Para cada promotor, los instaladores deberán sincronizarse en la instalación de los sensores, es decir, el promotor debe esperar a que finalicen todos sus instaladores antes de pasar a la siguiente etapa de casas. Para la ejecución de las tareas será necesario un marco `ExecutorService` y las herramientas de sincronización que nos proporciona. Para la solución se tiene que utilizar los siguientes elementos ya programados:

- `Constantes` : Interface con las constantes y tipos enumerados necesarios para la solución del ejercicio. Hay que utilizar los elementos de esta interface de forma obligatoria para la resolución.
- `Sensor`: Simula un tipo de sensor y también dispone de un método que simulará el tiempo necesario para su instalación.
- `Casa`: Simula donde se instalará un sensor. Dispone de una distribución de habitaciones según el `TipoCasa` que viene definido en `Constantes`.

El ejercicio consiste en completar la implementación de las siguientes clases:

- `Instalador`: Tiene un identificador, un número de sensores que instalar.
	- Creará una casa de `TipoCasa` aleatorio.
	- Para cada sensor que debe instalar:
		- Creará el sensor de un tipo aleatorio.
		- Añadirá el sensor a la casa si es posible, simulando el tiempo de instalación.
	- Debe programarse un procedimiento de interrupción.
	- Deberá indicar si ha finalizado la instalación correctamente o ha sido cancelada.
	- El resultado de su ejecución es la `Casa` con los sensores que haya creado y podido instalar.

- `Promotor`: Tiene un identificador, un número de etapas que cumplir.
	- El número de Instaladores vendrá determinado por `INSTALADORES`.
	- Para cada una de las etapas que tiene que cumplir:
		- Cada instalador tendrá que instalar un `NUM_SENSORES` con un rango aleatorio recogido en `VARIACION`.
		- El promotor ejecutará sus tareas de instalación programadas para esa etapa.
		- Esperará a que todos los instaladores hayan terminado la instalación de sus sensores.
		- Recogerá la Casa donde se han instalado los sensores y lo almacenará en una lista.
	- Se tiene que programar un procedimiento de interrupción. Tiene que interrumpir a los instaladores asignados y esperar a su finalización mediante el método `awaitTermination(.)`.
	- Deberá indicar si ha finalizado la instalación de todas sus etapas o ha sido interrumpido.
	- La lista de las casas es el resultado de su ejecución.

- `Hilo principal`: Realizará las siguientes tareas:
	- Creará un marco de ejecución con una capacidad de `PROMOTORES`.
		- Cada promotor tendrá un `NUM_ETAPAS`.
	- Añadir los promotores al marco `Executor`.
	- Espera a que el primer `Promotor` finalice la instalación de todas sus etapas.
	- Finalizar el marco `Executor` para que se cancele el resto de promotores.
	- Esperar a que todas las tareas finalicen mediante el método `awaitTermination(.)`.
	- Presentar la lista de casas que ha completado el `Promotor` antes de finalizar la ejecución del hilo principal.

<!--stackedit_data:
eyJoaXN0b3J5IjpbLTE3MTE2OTE1OTUsLTYyNTIwMzE4OSwxND
AyNjE3MTE3LDEwNDk2MzkwMywxNzYxNjU0NjExXX0=
-->